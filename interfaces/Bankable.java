package interfaces;

public interface Bankable {
    void finance();
}
