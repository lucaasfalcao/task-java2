package classes;

import interfaces.Bankable;

public class Apartment extends ShowcaseItem implements Bankable {
    private int pavements;
    private int rooms;
    private float area;
    private String localization;
    private Boolean availability;

    public Apartment(int pavements, int rooms, float area, String localization, Boolean availability) {
        super();
        this.setPavements(pavements);
        this.setRooms(rooms);
        this.setArea(area);
        this.setLocalization(localization);
        this.setAvailability(availability);
        this.setDiscount(discount);
    }

    public void setPavements(int pavements) {
        this.pavements = pavements;
    }

    public int getPavements() {
        return this.pavements;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getRooms() {
        return this.rooms;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public float getArea() {
        return this.area;
    }

    public void setLocalization(String localization) {
        this.localization = localization;
    }

    public String getLocalization() {
        return this.localization;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public Boolean getAvailability() {
        return this.availability;
    }

    public Boolean sell() {
        return this.availability = true;
    }

    public void saleInformation() {
        System.out.println("Apartamento à venda!");
    }

    public Boolean rent() {
        return this.availability = true;
    }

    public void rentInformation() {
        System.out.println("Apartamento disponível para aluguel!");
    }

    public Boolean reform() {
        return this.availability = false;
    }

    public void reformInformation() {
        System.out.println("Apartamento em reforma!");
    }

    @Override
    public String getDiscount() {
        return this.discount = "30%";
    }

    @Override
    public void finance() {
        System.out.println("Apartamento financiável!");
    }
}
