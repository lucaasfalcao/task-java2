package classes;

public class ShowcaseItem {
    protected String discount;
    protected String uploadDate;
    protected String saleDate;

    public ShowcaseItem() {
        this.setDiscount("10%");
        this.setUploadDate("13/09/2022");
        this.setSaleDate("07/09/2022");
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscount() {
        return this.discount;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getUploadDate() {
        return this.uploadDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getSaleDate() {
        return this.saleDate;
    }
}
